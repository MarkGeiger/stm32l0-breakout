#include "LPUart.h"

#include <cstring>

#include "stm32l0xx.h"

#include "stm32l0xx_hal.h"
#include "stm32l0xx_hal_gpio.h"
#include "stm32l0xx_hal_uart.h"

UART_HandleTypeDef hlpuart1;
DMA_HandleTypeDef hdma_lpuart1_rx;

bool initLPUartBaseRessources(UART_HandleTypeDef *uartHandle)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    __HAL_RCC_LPUART1_CLK_ENABLE();

    __HAL_RCC_GPIOC_CLK_ENABLE();
    GPIO_InitStruct.Pin = GPIO_PIN_10 | GPIO_PIN_11;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF0_LPUART1;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    /* LPUART1 DMA Init */
    /* LPUART1_RX Init */
    hdma_lpuart1_rx.Instance = DMA1_Channel3;
    hdma_lpuart1_rx.Init.Request = DMA_REQUEST_5;
    hdma_lpuart1_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hdma_lpuart1_rx.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_lpuart1_rx.Init.MemInc = DMA_MINC_ENABLE;
    hdma_lpuart1_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_lpuart1_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hdma_lpuart1_rx.Init.Mode = DMA_CIRCULAR;
    hdma_lpuart1_rx.Init.Priority = DMA_PRIORITY_LOW;
    if (HAL_DMA_Init(&hdma_lpuart1_rx) != HAL_OK)
    {
        return false;
    }

    __HAL_LINKDMA(uartHandle, hdmarx, hdma_lpuart1_rx);
    return true;
}

HAL_StatusTypeDef initLPUartBase(UART_HandleTypeDef *huart)
{
    /* Check the UART handle allocation */
    if (huart == nullptr)
    {
        return HAL_ERROR;
    }

    if (huart->gState == HAL_UART_STATE_RESET)
    {
        /* Allocate lock resource and initialize it */
        huart->Lock = HAL_UNLOCKED;
        if (!initLPUartBaseRessources(huart))
        {
            return HAL_ERROR;
        }
    }
    huart->gState = HAL_UART_STATE_BUSY;

    __HAL_UART_DISABLE(huart);
    /* Set the UART Communication parameters */
    if (UART_SetConfig(huart) == HAL_ERROR)
    {
        return HAL_ERROR;
    }

    if (huart->AdvancedInit.AdvFeatureInit != UART_ADVFEATURE_NO_INIT)
    {
        UART_AdvFeatureConfig(huart);
    }

    /* In asynchronous mode, the following bits must be kept cleared:
    - LINEN and CLKEN bits in the USART_CR2 register,
    - SCEN, HDSEL and IREN  bits in the USART_CR3 register.*/
    CLEAR_BIT(huart->Instance->CR2, (USART_CR2_LINEN | USART_CR2_CLKEN));
    CLEAR_BIT(huart->Instance->CR3, (USART_CR3_SCEN | USART_CR3_HDSEL | USART_CR3_IREN));

    __HAL_UART_ENABLE(huart);

    /* TEACK and/or REACK to check before moving huart->gState and huart->RxState to Ready */
    return (UART_CheckIdleState(huart));
}

bool LPUart::initLPUart()
{
    hlpuart1.Instance = LPUART1;
    hlpuart1.Init.BaudRate = 115200;
    hlpuart1.Init.WordLength = UART_WORDLENGTH_8B;
    hlpuart1.Init.StopBits = UART_STOPBITS_1;
    hlpuart1.Init.Parity = UART_PARITY_NONE;
    hlpuart1.Init.Mode = UART_MODE_TX_RX;
    hlpuart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    hlpuart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
    hlpuart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
    if (initLPUartBase(&hlpuart1) != HAL_OK)
    {
        return false;
    }
    return true;
}

uint16_t LPUart::getNumReceivedBytes(uint16_t bufferSize)
{
    return bufferSize - hlpuart1.hdmarx->Instance->CNDTR;
}

void LPUart::startDmaReceive(uint8_t *data, uint16_t size)
{
    HAL_UART_Receive_DMA(&hlpuart1, data, size);
}

void LPUart::abortDmaReceive()
{
    HAL_UART_AbortReceive(&hlpuart1);
}

void LPUart::debugPrint(const char _out[])
{
    HAL_UART_Transmit(&hlpuart1, (uint8_t *)_out, strlen(_out), 10);
}
