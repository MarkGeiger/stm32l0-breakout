#include "I2C.h"

#include "stm32l0xx_hal_dma.h"
#include "stm32l0xx_hal_gpio.h"
#include "stm32l0xx_hal_i2c.h"
#include "stm32l0xx_hal_rcc.h"

namespace
{
    I2C_HandleTypeDef hi2c1;
    constexpr uint8_t EEPROM_ADDRESS = 0xA0;
    constexpr uint8_t TEMP_SENSOR_ADDRESS = 0x90;
}

bool I2C::initI2C()
{
    __HAL_RCC_I2C1_CLK_ENABLE();

    // I2C Setup
    // Configure GPIO pins : PB6 (SCL) and PB7 (SDA)
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    GPIO_InitStruct.Pin = GPIO_PIN_6 | GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF1_I2C1;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    hi2c1.Instance = I2C1;
    hi2c1.Init.Timing = 0x00000708;
    hi2c1.Init.OwnAddress1 = 0;
    hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
    hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
    hi2c1.Init.OwnAddress2 = 0;
    hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
    hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
    hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;

    if (HAL_I2C_Init(&hi2c1) != HAL_OK)
    {
        return false;
    }

    if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
    {
        return false;
    }

    if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
    {
        return false;
    }
    return true;
}

void I2C::writeI2C(uint8_t address, uint8_t *data, uint8_t length)
{
    HAL_I2C_Master_Transmit(&hi2c1, address, data, length, 123200);
}

void I2C::readI2C(uint8_t address, uint8_t *dataBuffer, uint8_t dataSize)
{
    HAL_I2C_Master_Receive(&hi2c1, address, dataBuffer, dataSize, HAL_MAX_DELAY);
}

void I2C::initTempSensor()
{
    // 0x01 = config command
    // 0x00 = default config
    uint8_t data[2] = {0x01, 0x00};
    I2C::writeI2C(TEMP_SENSOR_ADDRESS, data, 2);
}

float I2C::readTemperature()
{
    uint8_t cmd[2];
    cmd[0] = 0x00; // command to read temp

    I2C::writeI2C(TEMP_SENSOR_ADDRESS, cmd, 1);
    I2C::readI2C( TEMP_SENSOR_ADDRESS, cmd, 2);
    return static_cast<float>(((cmd[0]<<8)|cmd[1]) / 256.0);
}

bool I2C::eepromWriteData(uint16_t memAddr, uint8_t *pData, uint16_t dataSize)
{
    while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
        ;

    if (HAL_I2C_Mem_Write(&hi2c1, EEPROM_ADDRESS, memAddr, I2C_MEMADD_SIZE_16BIT, pData, dataSize, HAL_MAX_DELAY) ==
        HAL_OK)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool I2C::eepromReadData(uint16_t memAddr, uint8_t *pData, uint16_t dataSize)
{
    while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
        ;

    if (HAL_I2C_Mem_Read(&hi2c1, EEPROM_ADDRESS, memAddr, I2C_MEMADD_SIZE_16BIT, pData, dataSize, HAL_MAX_DELAY) ==
        HAL_OK)
    {
        return true;
    }
    else
    {
        return false;
    }
}