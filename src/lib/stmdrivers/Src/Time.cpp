#include "Time.h"

#include "stm32l0xx_hal.h"

void Time::sleepMS(uint32_t ms)
{
    HAL_Delay(ms);
}

uint32_t Time::getMsSinceStart()
{
    return HAL_GetTick();
}