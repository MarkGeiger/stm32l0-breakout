#include "Uart.h"

#include "cstring"

#include "stm32l0xx.h"

#include "stm32l0xx_hal.h"
#include "stm32l0xx_hal_uart.h"
#include "stm32l0xx_hal_gpio.h"

UART_HandleTypeDef huart1;
DMA_HandleTypeDef hdma_usart1_rx;

bool initUartBaseRessources(UART_HandleTypeDef *huart)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    __HAL_RCC_USART2_CLK_ENABLE();
    GPIO_InitStruct.Pin = GPIO_PIN_2 | GPIO_PIN_3;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF4_USART1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    hdma_usart1_rx.Instance = DMA1_Channel5;
    hdma_usart1_rx.Init.Request = DMA_REQUEST_4;
    hdma_usart1_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hdma_usart1_rx.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_usart1_rx.Init.MemInc = DMA_MINC_ENABLE;
    hdma_usart1_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_usart1_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hdma_usart1_rx.Init.Mode = DMA_NORMAL;
    hdma_usart1_rx.Init.Priority = DMA_PRIORITY_LOW;
    if (HAL_DMA_Init(&hdma_usart1_rx) != HAL_OK)
    {
        return false;
    }

    __HAL_LINKDMA(huart, hdmarx, hdma_usart1_rx);
    return true;
}

HAL_StatusTypeDef initUartBase(UART_HandleTypeDef *huart)
{
    /* Check the UART handle allocation */
    if (huart == nullptr)
    {
        return HAL_ERROR;
    }

    if (huart->gState == HAL_UART_STATE_RESET)
    {
        /* Allocate lock resource and initialize it */
        huart->Lock = HAL_UNLOCKED;
        if (!initUartBaseRessources(huart))
        {
            return HAL_ERROR;
        }
    }
    huart->gState = HAL_UART_STATE_BUSY;

    __HAL_UART_DISABLE(huart);
    /* Set the UART Communication parameters */
    if (UART_SetConfig(huart) == HAL_ERROR)
    {
        return HAL_ERROR;
    }

    if (huart->AdvancedInit.AdvFeatureInit != UART_ADVFEATURE_NO_INIT)
    {
        UART_AdvFeatureConfig(huart);
    }

    /* In asynchronous mode, the following bits must be kept cleared:
    - LINEN and CLKEN bits in the USART_CR2 register,
    - SCEN, HDSEL and IREN  bits in the USART_CR3 register.*/
    CLEAR_BIT(huart->Instance->CR2, (USART_CR2_LINEN | USART_CR2_CLKEN));
    CLEAR_BIT(huart->Instance->CR3, (USART_CR3_SCEN | USART_CR3_HDSEL | USART_CR3_IREN));

    __HAL_UART_ENABLE(huart);

    /* TEACK and/or REACK to check before moving huart->gState and huart->RxState to Ready */
    return (UART_CheckIdleState(huart));
}

bool Uart::initUart()
{
    huart1.Instance = USART2;
    huart1.Init.BaudRate = 115200;
    huart1.Init.WordLength = UART_WORDLENGTH_8B;
    huart1.Init.StopBits = UART_STOPBITS_1;
    huart1.Init.Parity = UART_PARITY_NONE;
    huart1.Init.Mode = UART_MODE_TX_RX;
    huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart1.Init.OverSampling = UART_OVERSAMPLING_16;
    huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
    huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
    if (initUartBase(&huart1) != HAL_OK)
    {
        return false;
    }
    return true;
}

void Uart::debugPrint( const char _out[])
{
    HAL_UART_Transmit(&huart1, (uint8_t *) _out, strlen(_out), 10);
}

void Uart::transmitBlocking(uint8_t *pData, uint16_t size, uint32_t timeout)
{
    HAL_UART_Transmit(&huart1, pData, size, timeout);
}

uint16_t Uart::getNumReceivedBytes(uint16_t buffSize)
{
    return buffSize - huart1.hdmarx->Instance->CNDTR;
}

void Uart::receiveDma(uint8_t *pData, uint16_t size)
{
    HAL_UART_Receive_DMA(&huart1, pData, size);
}

void Uart::abortReceive()
{
    HAL_UART_AbortReceive(&huart1);
}
