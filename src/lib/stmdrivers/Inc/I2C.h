#pragma once

#include <cstdint>
class I2C
{
public:
    static bool initI2C();

    static void writeI2C(uint8_t address, uint8_t* data, uint8_t length);
    static void readI2C(uint8_t address, uint8_t* dataBuffer, uint8_t dataSize);

    static void initTempSensor();
    static float readTemperature();

    static bool eepromWriteData(uint16_t memAddr, uint8_t *pData, uint16_t dataSize);
    static bool eepromReadData(uint16_t memAddr, uint8_t *pData, uint16_t dataSize);
};