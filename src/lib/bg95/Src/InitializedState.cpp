#include "InitializedState.h"
#include "I2C.h"
#include "Numbers.h"
#include "Time.h"
#include "Uart.h"
#include <string>

void InitializedState::entry()
{
    m_activatedPdpContext = false;
    m_latitude = "0";
    m_longitude = "0";
    m_locationAcquired = false;
    m_signalStrengthErrorTime = 0;
    Uart::debugPrint("Initialized Modem\r\n");
}

uint8_t rssiToNegDbm(uint8_t rssi)
{
    if (rssi == 0)
    {
        return 113;
    }
    else if (rssi == 1)
    {
        return 111;
    }
    else if (rssi >= 2 && rssi <= 30)
    {
        return 109 - 2 * static_cast<int>(rssi);
    }
    else if (rssi == 31)
    {
        return 51;
    }
    else
    {
        // Handle out-of-range values
        return 0; // Return 0 or an appropriate default value
    }
}

EEvent InitializedState::update()
{
    if (!m_locationAcquired)
    {
        getLocation();
        m_locationAcquired = true;
    }

    auto rssi = m_modem->getRssi();
    if (rssi > 2 && rssi != 99 && m_modem->isModemConnectedToNetwork())
    {
        if (!m_activatedPdpContext)
        {
            // we seem to have a connection. Give modem some time to finish setup.
            Time::sleepMS(3000);

            // activate PDP Context
            m_activatedPdpContext = true;
            m_modem->transmitAtc("AT+QIACT=1\r\n", 150000); // maximum response 150s
            Time::sleepMS(1000);
        }

        // we have a somewhat reasonable signal quality
        uint8_t negDbm = rssiToNegDbm(rssi);

        // get battery level
        m_modem->transmitAtc("AT+CBC\r\n");
        uint16_t size = 0;
        uint8_t * buf = m_modem->getLine(size);
        std::string line = std::string(reinterpret_cast<char *>(buf), size);
        auto batteryPercentage = Numbers::extractNumber<uint8_t>(line, 0, 1);
        float temperature = I2C::readTemperature();

        // send package
        m_modem->transmitAtc("AT+QIOPEN=1,0,\"UDP\",\"94.16.106.227\",7678,0,2\r\n", 150000,
                             "CONNECT"); // maximum response 150s
        Time::sleepMS(2000);

        static constexpr uint16_t transparentResponseTime = 20;
        m_modem->transmitAtc(m_modem->getImei(), transparentResponseTime); // sensor id
        m_modem->transmitAtc(",", transparentResponseTime);
        m_modem->transmitAtc(m_latitude.c_str(), transparentResponseTime);
        m_modem->transmitAtc(",", transparentResponseTime);
        m_modem->transmitAtc(m_longitude.c_str(), transparentResponseTime);
        m_modem->transmitAtc(",", transparentResponseTime);
        m_modem->transmitAtc(std::to_string(batteryPercentage).c_str(), transparentResponseTime);
        m_modem->transmitAtc(",", transparentResponseTime);
        m_modem->transmitAtc(std::to_string(negDbm).c_str(), transparentResponseTime);
        m_modem->transmitAtc(",", transparentResponseTime);
        m_modem->transmitAtc(Numbers::floatToString(temperature).c_str(), transparentResponseTime);
        m_modem->transmitAtc("\r\n", transparentResponseTime);

        Time::sleepMS(1200);
        m_modem->transmitAtc("+++");
        Time::sleepMS(1200);
        m_modem->transmitAtc("AT+QICLOSE=0\r\n");

        m_locationAcquired = false;
        m_signalStrengthErrorTime = 0;
    }
    else
    {
        // count time since first error case.
        if (m_signalStrengthErrorTime == 0)
        {
            m_signalStrengthErrorTime = Time::getMsSinceStart();
        }

        // If we cannot recover within 10 minutes
        // The initial scanning of the network may take long time, until modem remembers local network
        if (Time::getMsSinceStart() - m_signalStrengthErrorTime > 60 * 1000 * 10)
        {
            if (!m_modem->transmitAtc("AT+CFUN=1,1\r\n", 15000, "OK"))
            {
                m_modem->resetModule();
                return EEvent::RESET_MODEM;
            }
        }
    }

    return EEvent::NONE;
}
void InitializedState::getLocation()
{
    // acquire GPS location
    m_modem->transmitAtc("AT+QGPS=1,1\r\n", 2000);
    uint32_t start = Time::getMsSinceStart();
    while (Time::getMsSinceStart() - start < 300000) // try until fix or timeout
    {
        if (m_modem->transmitAtc("AT+QGPSLOC=2\r\n", 300, "QGPSLOC"))
        {
            // we have a fix read information
            uint16_t size = 0;
            uint8_t * buf = m_modem->getLine(size);
            std::string line = std::string(reinterpret_cast<char *>(buf), size);

            m_latitude = Numbers::extractNumber<std::string>(line, "0", 1);
            m_longitude = Numbers::extractNumber<std::string>(line, "0", 2);

            if (!m_latitude.empty() && !m_longitude.empty())
            {
                break;
            }
        }
        Time::sleepMS(3000);
    }
    m_modem->transmitAtc("AT+QGPSEND\r\n");
}

void InitializedState::exit()
{
    // deactivate PDP Context
    if (m_activatedPdpContext)
    {
        m_modem->transmitAtc("AT+QIDEACT=1\r\n", 40000); // maximum response 40s
        Time::sleepMS(1000);
    }
}
