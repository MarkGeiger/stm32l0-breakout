#include "UninitializedState.h"

#include "Time.h"

void UninitializedState::entry()
{
    // initialize
    m_modem->transmitAtc("ATE0\r\n");
    m_modem->transmitAtc("AT+QGMR\r\n");
    m_modem->readImei();
    m_modem->transmitAtc("AT+CFUN=0\r\n");
    m_modem->transmitAtc("AT+COPS=0\r\n");

    // following code will first read back the wanted configuration and only write it if it is not already set
    // this is done because the modem will trigger a network rescan if the configuration is changed

    uint16_t length = 0;
    m_modem->transmitAtc("AT+QCFG=\"nwscanseq\"\r\n");
    uint8_t *str = m_modem->getLine(length);
    if (std::string(reinterpret_cast<char *>(str), length) != "+QCFG: \"nwscanseq\",020301\r\n")
    {
        // first search eMTC, then NB-IoT, then GSM
        m_modem->transmitAtc("AT+QCFG=\"nwscanseq\",020301\r\n");
    }

    m_modem->transmitAtc("AT+CGDCONT?\r\n");
    str = m_modem->getLine(length);
    if (std::string(reinterpret_cast<char *>(str), length) !=
        "+CGDCONT: 1,\"IP\",\"iot.1nce.net\",\"0.0.0.0\",0,0,0\r\n")
    {
        // set 1nce APN
        m_modem->transmitAtc("AT+CGDCONT=1,\"IP\",\"iot.1nce.net\"\r\n");
    }

    m_modem->transmitAtc("AT+QCFG=\"nwscanmode\"\r\n");
    str = m_modem->getLine(length);
    if (std::string(reinterpret_cast<char *>(str), length) != "+QCFG: \"nwscanmode\",0\r\n")
    {
        // automatic RAT search
        m_modem->transmitAtc("AT+QCFG=\"nwscanmode\",0\r\n");
    }

    m_modem->transmitAtc("AT+QCFG=\"iotopmode\"\r\n");
    str = m_modem->getLine(length);
    if (std::string(reinterpret_cast<char *>(str), length) != "+QCFG: \"iotopmode\",2\r\n")
    {
        m_modem->transmitAtc("AT+QCFG=\"iotopmode\",2\r\n");
    }

    m_modem->transmitAtc("AT+QCFG=\"band\"\r\n");
    str = m_modem->getLine(length);
    if (std::string(reinterpret_cast<char *>(str), length) != "+QCFG: \"band\",0xf,0x80084,0x80084\r\n")
    {
        m_modem->transmitAtc("AT+QCFG=\"band\",F,80084,80084\r\n");
    }

    m_modem->transmitAtc("AT+QCFG=\"fast/poweroff\"\r\n");
    str = m_modem->getLine(length);
    if (std::string(reinterpret_cast<char *>(str), length) != "+QCFG: \"fast/poweroff\",25,1\r\n")
    {
        m_modem->transmitAtc("AT+QCFG=\"fast/poweroff\",25,1\r\n");
    }
}

EEvent UninitializedState::update()
{
    if (!m_modem->transmitAtc("AT+CFUN=1\r\n", 15000, "OK"))
    {
        return EEvent::RESET_MODEM;
    }

    uint32_t start = Time::getMsSinceStart();
    while (!m_modem->isModemConnectedToNetwork())
    {
        Time::sleepMS(2000);
        if (Time::getMsSinceStart() - start > 60 * 1000 * 10)
        {
            // could not get network for more than 10 minutes. Try a soft reset
            m_modem->transmitAtc("AT+CFUN=1,1\r\n", 15000, "OK");
            start = Time::getMsSinceStart();
        }
    }

    // may skip XTRA state and directly switch to initialized state
    return EEvent::INITIALIZE_XTRA;
}

void UninitializedState::exit()
{
}
