#include "UnknownState.h"

#include "Uart.h"

void UnknownState::entry()
{

}

EEvent UnknownState::update()
{
    // TODO: Note. It is not recommended to determine the state of the modem by sending AT commands.
    // Rather use STATUS and APP READY PINS

    uint8_t counter = 0;
    for (int i = 0; i < 10; i++)
    {
        if (m_modem->transmitAtc("AT\r\n"))
        {
            counter++;
        }
    }

    if (counter == 10)
    {
        return EEvent::POWER_ON;
    }

    // else initiate power on procedure
    Uart::debugPrint("Modem offline\r\n");
    return EEvent::MODEM_NOT_RESPONDING;
}

void UnknownState::exit()
{

}
