#pragma once

#include "Statemachine.h"
#include "BG95.h"

enum EState
{
    ALL,
    UNKNOWN,
    POWERED_OFF,
    UNINITIALIZED,
    GNSS_XTRA,
    INITIALIZED,
};

enum EEvent
{
    NONE,
    MODEM_NOT_RESPONDING,
    POWER_ON,
    INITIALIZED_MODEM,
    INITIALIZE_XTRA,
    RESET_MODEM
};

class ModemStatemachine : public Statemachine<EState, EEvent>
{
public:
    ModemStatemachine();
};