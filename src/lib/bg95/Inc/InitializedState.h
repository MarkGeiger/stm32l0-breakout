#pragma once

#include "ModemStatemachine.h"
#include "IModemState.h"

class InitializedState : public IModemState<EState, EEvent>
{
public:
    explicit InitializedState(BG95 &modem) : IModemState<EState, EEvent>(EState::INITIALIZED, modem)
    {}

    void entry() override;

    EEvent update() override;

    void exit() override;

private:
  void getLocation();

    bool m_activatedPdpContext = false;
    std::string m_longitude = "0";
    std::string m_latitude = "0";
    uint32_t m_signalStrengthErrorTime = 0;
    bool m_locationAcquired = false;
};