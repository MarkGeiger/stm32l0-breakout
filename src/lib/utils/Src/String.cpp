#include "String.h"

std::string String::RemoveChar(const std::string& str, char c)
{
    std::string result;
    for (char currentChar : str)
    {
        if (currentChar != c)
            result += currentChar;
    }
    return result;
}
