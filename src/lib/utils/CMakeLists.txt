set(DIR ${CMAKE_CURRENT_SOURCE_DIR})
file(GLOB LIB_SOURCES
        ${DIR}/Src/*.cpp
        ${DIR}/Inc/*.h
)

add_library(utils STATIC ${LIB_SOURCES})
target_include_directories(utils PUBLIC ./Inc/)

if (!CMAKE_TOOLCHAIN_FILE)
    add_subdirectory(Test)
endif ()
