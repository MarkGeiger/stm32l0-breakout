# PlainSense Gateway - Firmware

## Prerequisites

- download and install arm-none-eabi compiler: https://developer.arm.com/downloads/-/gnu-rm
- add arm-none-eabi compiler binaries to $PATH

## Building

How to build the sources:

### Build for target platform:
```
mkdir build
cd build
cmake .. -DCMAKE_TOOLCHAIN_FILE=../CMake/GNU-ARM-Toolchain.cmake
make
```

### Build simulation:
```
mkdir build
cd build
cmake ..
make
```

### Building unit test projects:
```
> navigate to unit test: e.g. ./src/lib/statemachine/Test

mkdir build
cd build
cmake ..
make

> then run test executable ./Statemachine_Test
```

Unit test projects are separate standalone CMakeFiles that are meant to 
be compiled on the development platform, this helps to increase development speed since 
no target platform is needed. Take a look at the .gitlab-ci.yml and the docker/Dockerfile to get a better idea about requirements and setup.

