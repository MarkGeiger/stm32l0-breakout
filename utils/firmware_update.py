import argparse
import os
import struct
import serial
import time


def send_data_size(serial_port, file_path):
    # Get the size of the file in bytes
    file_size = os.path.getsize(file_path)

    # Pack the file size into a 4-byte binary format (unsigned long)
    size_bytes = file_size.to_bytes(4, byteorder="little")

    print(size_bytes)
    serial_port.write(size_bytes)
    print(f"Sent file size: {file_size} bytes")


def send_file_data(serial_port, file_path):
    chunk_size = 512
    file_size = os.path.getsize(file_path)
    with open(file_path, 'rb') as file:
        while file_size > 0:
            while True:
                ack = serial_port.readline().decode().strip()
                if ack == "+":
                    break

            # Read a chunk of data from the file
            chunk = file.read(chunk_size)

            # Break if no more data to send
            if not chunk:
                break

            # Send the chunk
            serial_port.write(chunk)
            file_size = file_size - len(chunk)

            print(f"Sent {len(chunk)} bytes of file data, bytes left: {file_size}")


def main():
    # Parse command line arguments
    parser = argparse.ArgumentParser(description="Serial communication script")
    parser.add_argument("port", help="Serial port path, e.g., /dev/ttyUSB0")
    parser.add_argument("file_path", help="Path to the file for which size will be sent")
    args = parser.parse_args()

    # Open the serial port
    with serial.Serial(args.port, baudrate=115200, timeout=1) as ser:
        # Wait for the catchphrase
        while True:
            line = ser.readline().decode().strip()
            print(line)
            if line == "Waiting for app update...":
                break

        # Wait for 1 second
        time.sleep(1)

        # Send the update request
        ser.write("Update please\r\n".encode())
        print("Sent 'Update please'")

        # Wait to receive the message "data_size?"
        while True:
            line = ser.readline().decode().strip()
            print(line)
            if line == "data_size?":
                break

        # Send the file size
        send_data_size(ser, args.file_path)

        # Wait for acknowledgment
        send_file_data(ser, args.file_path)
        print('Update finished')


if __name__ == "__main__":
    main()
