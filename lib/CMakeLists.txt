set(sources
        pure_virtual.c
        STM32L0xx_HAL_Driver/Inc/stm32l0xx_hal.h
        STM32L0xx_HAL_Driver/Src/stm32l0xx_hal.c
        STM32L0xx_HAL_Driver/Src/stm32l0xx_hal_gpio.c
        STM32L0xx_HAL_Driver/Src/stm32l0xx_hal_rcc.c
        STM32L0xx_HAL_Driver/Src/stm32l0xx_hal_rcc_ex.c
        STM32L0xx_HAL_Driver/Src/stm32l0xx_hal_cortex.c
        STM32L0xx_HAL_Driver/Src/stm32l0xx_hal_uart.c
        STM32L0xx_HAL_Driver/Src/stm32l0xx_hal_flash.c
        STM32L0xx_HAL_Driver/Src/stm32l0xx_hal_flash_ex.c
        STM32L0xx_HAL_Driver/Src/stm32l0xx_hal_dma.c
        STM32L0xx_HAL_Driver/Src/stm32l0xx_hal_rtc.c
        STM32L0xx_HAL_Driver/Src/stm32l0xx_hal_i2c.c
        STM32L0xx_HAL_Driver/Src/stm32l0xx_hal_i2c_ex.c
        STM32L0xx_HAL_Driver/Src/stm32l0xx_hal_rtc_ex.c
        STM32L0xx_HAL_Driver/Src/stm32l0xx_hal_pwr.c
        STM32L0xx_HAL_Driver/Src/stm32l0xx_hal_pwr_ex.c
)

add_library(stm32l0xx ${sources})

target_compile_definitions(stm32l0xx PRIVATE -DSTM32L053xx)

# add include directories for standard libraries
target_include_directories(stm32l0xx PUBLIC CMSIS/Device/ST/STM32L0xx/Include/)
target_include_directories(stm32l0xx PUBLIC CMSIS/Include/)
target_include_directories(stm32l0xx PUBLIC STM32L0xx_HAL_Driver/Inc/)
target_include_directories(stm32l0xx PUBLIC conf/)

# additional compiler options: use size-optimized version of library in release build, use -O0 in debug build
if(CMAKE_BUILD_TYPE MATCHES Debug)
    set(additional_flags -O0)
else()
    set(additional_flags -Os)
endif()